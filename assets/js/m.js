

console.log('Iniciamos');




const clasebien = 'bien';
const clasemal = 'mal';

function manejadorClases(i,b,m){
    i.classList.remove(clasemal);
    b ? i.classList.add(clasebien) : i.classList.remove(clasebien);
    if(m) i.classList.add(clasemal);
}

/**
 *  Función para validar campos
 *
 * @param {event} event El evento que lo dispara, ahorita es "input"
 * @param {element} item El elemento en html
 * @return {void} No regresa nada
 */
function agregarEvento(event, item){
    let valor = item.value;
    let re = '';
    if (valor.length >= 4) {
        
        // Validmos campos y agregamos clases
        switch (item.type) {
            // Tipo buscador
            case 'text':
                (valor) ? manejadorClases(item, true, false) : manejadorClases(item, false, true);
                break;

            // Color
            case 'color' :
                (valor) ? manejadorClases(item, true, false) : manejadorClases(item, false, true);
                break;
        
            // Correo electrónico
            default :
                re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                ( re.test(String(valor.toLowerCase())) ) ? manejadorClases(item, true, false): manejadorClases(item, false, true);
                break;


        }

    } else {
        manejadorClases(item, false, false);
    }
}



// Procesamiento de campos
document.querySelectorAll('.campo').forEach(item => {
    item.addEventListener('input', function(event){ agregarEvento(event,item) });
});


// Desactivar enter
window.addEventListener('keydown',function(e){if(e.keyIdentifier=='U+000A'||e.keyIdentifier=='Enter'||e.keyCode==13){if(e.target.nodeName=='INPUT'&&e.target.type=='text'){e.preventDefault();return false;}}},true);

// Siguiente
document.querySelectorAll('.boton').forEach(item => {
    item.addEventListener('click', function(event){ 


        if(item.previousElementSibling.classList.contains('bien')) {
            var fieldsets = document.querySelectorAll(".wrap");
            for (var i=0; i < fieldsets.length; i++) 
            {    
                fieldsets[i].classList.remove('visible');
            }
            item.parentElement.classList.add('invisible')
            item.parentElement.nextElementSibling.classList.add('visible');
        } else {
            alert('Completa correctamente el campo');
        }

    });

});




function enviarformulario(e){
    e.preventDefault();
    var bandera = 0;

    document.getElementById('benviar').setAttribute("disabled",true);
    document.getElementById("benviar").innerHTML = "Espera un momento...";
    
    // Validamos si están bien llenos
    if(document.querySelectorAll('.bien').length == 3) {

        let datos = [];
        document.querySelectorAll('.bien').forEach(element => {
            datos.push(element.value);
        });
        
        var xmlhttp = new XMLHttpRequest();
		xmlhttp.open("POST","p.php",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        xmlhttp.send("nombre="+datos[0]+"&email="+datos[1]+"&color="+datos[2]);
        
        document.getElementById('nuser').textContent = datos[0];

        xmlhttp.onreadystatechange=function(){

            var resultado = JSON.parse(xmlhttp.response);

            if(resultado.code==1 && !bandera) {
                bandera++;

                var xhttp = new XMLHttpRequest();
                xhttp.open("GET", "g.php", true);
                xhttp.send();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        
                        var respuesta = JSON.parse(xhttp.response);
                        var tabla = document.getElementById('contenido');
                        
                        for (let index = 0; index < respuesta.length; index++) {

                            var fila = document.createElement("tr");

                            var col1 = document.createElement('td');
                            col1.appendChild(document.createTextNode(respuesta[index].nombre));
                            var col2 = document.createElement('td');
                            col2.appendChild(document.createTextNode(respuesta[index].email));
                            var col3 = document.createElement('td');
                            var col3span = document.createElement('span');
                            col3span.style.backgroundColor = respuesta[index].color;
                            col3.appendChild(col3span);
                            
                            fila.appendChild(col1);
                            fila.appendChild(col2);
                            fila.appendChild(col3);

                           tabla.appendChild(fila);
                        }

                        document.getElementById('modal').classList.add('visible');
                        gtag('event', 'Objetivo formulario', { 'event_category': 'PHP y JS', 'event_label': '4'})
                    } else {
                        
                    }
                };


            } else {
            }

        }



    } else {
        alert('Hay un error en los datos, por favor revisa nuevamente el formulario. No olvides llenar bien la hora')
    }
}