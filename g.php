<?php

    include_once('conf/conf.php');

    $servername = SERVIDOR;
    $database = BASEDATOS;
    $username = USUARIO;
    $password = CONTRASENA;

    $conn = mysqli_connect($servername, $username, $password, $database);
    mysqli_set_charset($conn, "utf8");

    
    $sql = "SELECT * FROM (SELECT * FROM datos ORDER BY id DESC LIMIT 5) `datos` WHERE 1";
    $resultado=$conn->query($sql);


    $datos = array();
    
    while($fila = $resultado->fetch_assoc()) {
        $arregloTemp = array();
        $arregloTemp['id'] = $fila['id'];
        $arregloTemp['nombre'] = $fila['nombre'];
        $arregloTemp['email'] = $fila['email'];
        $arregloTemp['color'] = $fila['color'];

        array_push($datos, $arregloTemp);
    }

    echo json_encode($datos);


?>